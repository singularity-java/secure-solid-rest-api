package bankapp.auth;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomUserRepository extends JpaRepository<CustomUser, Long> {

    CustomUser findCustomUserByUsername(String username);
}
