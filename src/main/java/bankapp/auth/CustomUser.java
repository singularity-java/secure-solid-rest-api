package bankapp.auth;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CustomUser {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE
    )
    private Long id;
    private String username;
    private String password;

    public CustomUser(CustomUser user) {
        this(user.id, user.username, user.password);
    }

    public CustomUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
