package bankapp.exception;

import org.springframework.http.HttpStatus;

public class AlreadyHasThatUsernameException extends RuntimeException{
    public AlreadyHasThatUsernameException(String username) {
        super("User with username " + username + " already exists");
    }
}
