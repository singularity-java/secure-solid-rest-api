package bankapp.controller;

import bankapp.account.Account;
import bankapp.account.AccountId;
import bankapp.response.AccountActionResponse;
import bankapp.response.AccountCreationResponse;
import bankapp.service.*;
import bankapp.transaction.Transaction;
import bankapp.transaction.TransactionDeposit;
import bankapp.transaction.TransactionListingService;
import bankapp.transaction.TransactionWithdraw;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path="/accounts")
@AllArgsConstructor
public class AccountController {
    private AccountListingService accountListingService;
    private AccountDeletionService accountDeletionService;
    private AccountCreationServiceWeb accountCreationService;
    private TransactionDeposit transactionDeposit;
    private TransactionWithdraw transactionWithdraw;
    private TransactionListingService transactionListingService;

    @GetMapping
    public List<Account> getStudents() {
        return accountListingService.getClientAccounts("1");
    }

    @PostMapping
    public ResponseEntity<Object> addNewAccount(@RequestBody Account account) {
        Long accountId;
        try {
            accountId = accountCreationService.createAccount(account);

        } catch (Exception e) {
            return new ResponseEntity<>(new AccountActionResponse("Account not specified correctly", HttpStatus.BAD_REQUEST), HttpStatus.BAD_REQUEST);
        }
        String accountNumber = String.format("%03d%06d", 1, Integer.parseInt(accountId+""));
        return new ResponseEntity<>(new AccountCreationResponse("Account " + accountNumber + " created", account, HttpStatus.OK), HttpStatus.OK);

    }
    @GetMapping("/{accountId}")
    public Account getAccountById(@PathVariable("accountId") Long accountId) {
        return accountListingService.getClientAccountById("1", accountId);
    }

    @DeleteMapping("/{accountId}")
    public ResponseEntity<Object> deleteAccountById(@PathVariable("accountId") Long accountId) {
        accountDeletionService.deleteAccountById(accountId);
        return new ResponseEntity<>(new AccountActionResponse("Account " + accountId + " deleted", HttpStatus.OK), HttpStatus.OK);
    }

    @PostMapping("/{accountId}/deposit")
    public ResponseEntity<Object> depositAccount(@PathVariable("accountId") Long accountId, @RequestBody Double amount) {
        Account account = accountListingService.getClientAccount("1", accountId);
        transactionDeposit.execute(amount, account);
        return new ResponseEntity<>(new AccountActionResponse(
                "$" + amount + " transferred to account " + accountId, HttpStatus.OK), HttpStatus.OK);
    }
    @PostMapping("/{accountId}/withdraw")
    public ResponseEntity<Object> withdrawAccount(@PathVariable("accountId") Long accountId, @RequestBody Double amount) {
        Account account = accountListingService.getClientAccount("1", accountId);
        transactionWithdraw.execute(amount, account);
        return new ResponseEntity<>(new AccountActionResponse(
                "$" + amount + " transferred from account " + accountId, HttpStatus.OK), HttpStatus.OK);
    }
    @GetMapping("/{accountId}/transactions")
    public List<Transaction> getTransactionsByAccountId(@PathVariable("accountId") Long accountId) {
        return transactionListingService.getTransactionsByAccountId(accountId);
    }

    @PostMapping("/{accountId}/transfer")
    public ResponseEntity<Object> transferMoney(@PathVariable("accountId") Long accountId, @RequestBody AccountId destinationAccountIdAmount) {
        Account account = accountListingService.getClientAccount("1", accountId);
        Long destinationAccountId = destinationAccountIdAmount.getId();
        Account destinationAccount = accountListingService.getClientAccount("1", destinationAccountId);
        Double amount = destinationAccountIdAmount.getAmount();


        transactionWithdraw.execute(amount, account);
        transactionDeposit.execute(amount, destinationAccount);


        return new ResponseEntity<>(new AccountActionResponse(
                "$" + amount + " transferred from account " + accountId + " to account " + destinationAccountId,
                HttpStatus.OK), HttpStatus.OK);
    }
}
