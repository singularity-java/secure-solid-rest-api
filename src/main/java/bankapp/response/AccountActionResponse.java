package bankapp.response;

import bankapp.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class AccountActionResponse {
    private String message;
    private HttpStatus statusCode;
}
