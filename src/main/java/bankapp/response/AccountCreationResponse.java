package bankapp.response;

import bankapp.account.Account;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class AccountCreationResponse {
    private String message;
    private Account account;
    private HttpStatus statusCode;
}
