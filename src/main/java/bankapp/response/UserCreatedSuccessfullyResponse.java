package bankapp.response;

import bankapp.auth.CustomUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
@AllArgsConstructor
@Data
public class UserCreatedSuccessfullyResponse {
    private String message;
    private CustomUser customUser;
    private HttpStatus statusCode;
}
