package bankapp;

import bankapp.cli.AccountBasicCLI;
import bankapp.cli.MyCLI;
import bankapp.cli.TransactionDepositCLI;
import bankapp.cli.TransactionWithdrawCLI;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Scanner;

@SpringBootApplication
@SecurityScheme(name = "basicauth", scheme = "bearer", type = SecuritySchemeType.HTTP, in = SecuritySchemeIn.HEADER, bearerFormat = "JWT")
public class DemoApplication implements CommandLineRunner {
	@Autowired
	private AccountBasicCLI accountBasicCLI;
	@Autowired
	private  MyCLI myCLI;
	@Autowired
	private TransactionDepositCLI transactionDepositCLI;
	@Autowired
	private TransactionWithdrawCLI transactionWithdrawCLI;
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class);
	}
	@Override
	public void run(String... arg0) throws Exception {

		String clientId = "1";

		printHelp();
		boolean running = true;
		Scanner scanner = myCLI.getScanner();
		while(running) {
			switch (scanner.nextLine()) {
				case "1":
					accountBasicCLI.getAccounts(clientId);
					break;
				case "2":
					accountBasicCLI.createAccountRequest(clientId);
					break;
				case "3":
					transactionDepositCLI.depositMoney(clientId); break;
				case "4":
					transactionWithdrawCLI.withdrawMoney(clientId); break;
				case "6":
					printHelp();
					break;
				case "7":
					System.out.println("Application closed");
					running = false;
					System.exit(0);
					break;
				default:
					System.out.println("Error: invalid command");
			}
		}
		scanner.close();
	}

	public static void printHelp() {
		System.out.println("Welcome to CLI bank serviceEnter operation number:\n" +
				"1 - show accounts\n" +
				"2 - create account\n" +
				"3 - deposit\n" +
				"4 - withdraw\n" +
				"5 - transfer\n" +
				"6 - this message\n" +
				"7 - exit\n");
	}
}
