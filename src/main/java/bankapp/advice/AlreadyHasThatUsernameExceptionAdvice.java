package bankapp.advice;

import bankapp.exception.AlreadyHasThatUsernameException;
import bankapp.response.AlreadyHasThatUsernameResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class AlreadyHasThatUsernameExceptionAdvice {
    @ResponseBody
    @ExceptionHandler(AlreadyHasThatUsernameException.class)
    public ResponseEntity<Object> handleSameUsername(AlreadyHasThatUsernameException e) {
        System.out.println("im here");
        AlreadyHasThatUsernameResponse response =
                new AlreadyHasThatUsernameResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
