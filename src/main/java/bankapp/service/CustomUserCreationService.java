package bankapp.service;

import bankapp.auth.CustomUser;

import java.util.List;

public interface CustomUserCreationService {
    public void createUser(CustomUser customUser);

    public CustomUser getCustomUserByUsername(String username);

    public List<CustomUser> getAllCustomUsers();
}
